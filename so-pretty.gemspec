lib_path  = "./lib"
spec_path = "./spec"
bin_path  = "./bin"

libs  = Dir.glob("#{lib_path}/**/*.rb")
libs += Dir.glob("#{lib_path}/**/*.erb")

specs = Dir.glob("#{spec_path}/**/*.rb")
bins  = Dir.glob("#{bin_path}/so-pretty")
bins += Dir.glob("#{bin_path}/sp")

flist = libs + specs + bins
flist << "so-pretty.gemspec"
flist << "README.md"

require_relative './lib/so-pretty/version'

summary = <<-EOD
A gem to pretty print anything.  Currently supports input formats:

- ruby (.inspect formatted)
- yaml
- json

And will auto-detect input type, though you can force detection.

Outputs yaml by default, but can output ruby, yaml, or json.  Input streams
or files may be compressed with zlib/deflate, however, gzip is not currently
supported.
EOD

Gem::Specification.new do |gem|
  gem.name          = "so-pretty"
  gem.version       = SoPretty::VERSION
  gem.authors       = ["Brett Nakashima"]
  gem.email         = ["brettnak@gmail.com"]
  gem.summary       = "SoPretty print anything"
  gem.description   = summary

  # TODO: Rename this repo to match the gem name
  gem.homepage      = "http://gitlab.com/brettnak/pretty"

  gem.files         = flist
  gem.executables   = bins.map{ |f| File.basename(f) }
  gem.test_files    = specs
  gem.require_paths = ["lib/"]

  gem.add_development_dependency 'rspec', '~> 3.1'
  gem.add_development_dependency 'awesome_print'

  gem.license = "MIT"
end
