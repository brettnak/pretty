#!/usr/bin/env ruby

require 'rubygems'
require 'json'
require 'zlib'
require 'yaml'
require 'optparse'
require 'so-pretty/version'

begin
  require 'awesome_print'
rescue LoadError => e
  STDERR.puts "awesome_print is not installed.  Install it to get better printing from the :ruby format.  `gem install awesome_print`"
end

module SoPretty
  class Base
    def initialize( options = {} )
      @instream  = to_stream( :input,  options[:instream] )
      @outstream = to_stream( :output, options[:outstream] )
      @outformat = options[:format]
    end

    def prettify
      if @outformat == :yaml
        @outstream.puts YAML::dump( parsed )
        return
      end

      if @outformat == :ruby
        if defined?( AwesomePrint )
          @outstream.puts parsed.ai( plain: true, index: false )
        else
          @outstream.puts parsed.inspect
        end

        return
      end

      if @outformat == :minjson
        @outstream.puts JSON.dump( parsed )
        return
      end

      @outstream.puts JSON.pretty_generate( parsed )
    ensure
      close_streams
    end

    def parsed
      return @parsed if @parsed

      begin
        data = JSON.load( string )
        return @parsed = data
      rescue JSON::ParserError => e
      end

      begin
        data = YAML::load( string )
        return @parsed = data
      rescue Psych::SyntaxError => e
      end

      begin
        data = Kernel.eval( string )
        return @parsed = data
      rescue StandardError => e
      end

      raise ArgumentError, "Input was unparsable as JSON or YAML."
    end

    def string
      return @string if @string

      str = @instream.read

      zstream = Zlib::Inflate.new
      begin
        buf = zstream.inflate(str)
        zstream.finish
        zstream.close

        return @string = buf
      rescue Exception => e
      end

      return @string = str
    end

    def close_streams
      @instream.flush
      @instream.close

      @outstream.flush
      @outstream.close
    end

    def self.from_cli( argv )
      options = {
        :instream  => "-",
        :outstream => "-",
        :format    => :yaml,
      }

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: sp [options]"
        opts.separator ""
        opts.separator "Options:"

        opts.on( "-i", "--input [instream]", "Input stream, one of: '-', 'STDIN', or a file name. default: '-'" ) do |instream|
          options[:instream] = instream
        end

        opts.on( "-o", "--output [outstream]", "Output stream, one of: '-', 'STDOUT', 'STDERR' or a file name. default: '-'" ) do |outstream|
          options[:outstream] = outstream
        end

        opts.on( "-f", "--format [format]", "Output format, one of: 'yaml', 'json', 'ruby', 'minjson'") do |format|
          options[:format] = format.to_sym
        end

        opts.on("-V", "--version", "Show version") do
          puts VERSION
          exit
        end

        opts.separator ""
        opts.separator "Examples:"
        opts.separator EXAMPLES
      end

      parser.parse( argv )

      return self.new( options )
    end

    private
    def to_stream( input_output, maybe_stream )
      case maybe_stream.downcase
      when "stdin"
        raise ArgumentError, "Output stream cannot be STDIN"
        return STDIN
      when "stdout"
        raise ArgumentError, "Input stream cannot be STDOUT"
        return STDOUT
      when "stderr"
        raise ArgumentError, "Input stream cannot be STDERR"
        return STDERR
      end

      if maybe_stream == "-"
        if input_output == :input
          return STDIN
        end

        return STDOUT
      end

      mode = input_output == :input ? "r" : "w"
      return File.open( maybe_stream, mode )
    end
  end

  EXAMPLES=<<EOD
    $ echo '{"foo":"bar"}' | sp -f ruby
    > {
    >     "foo" => "bar"
    > }

    $ echo '{"foo":"bar"}' | sp -f json
    > {
    >   "foo": "bar"
    > }

    $ echo '{"foo":"bar"}' | sp -f yaml
    > ---
    > foo: bar

    $ echo '{"foo":"bar"}' | sp -f minjson
    > {"foo":"bar"}
EOD
end
