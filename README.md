## Usage

```
Usage: sp [options]
    -i, --input [instream]           Input stream, one of: '-', 'STDIN', or a file name
    -o, --output [outstream]         Output stream, one of: '-', 'STDOUT', 'STDERR' or a file name
    -f, --format [format]            Output format, one of: 'yaml', 'json', 'ruby', 'minjson'
```

## TODO

- add formats
  * csv
  * toml
  * some specs would be nice

## Examples

    $ echo '{"foo":"bar"}' | sp -f ruby
    > {
    >     "foo" => "bar"
    > }

    $ echo '{"foo":"bar"}' | sp -f json
    > {
    >   "foo": "bar"
    > }

    $ echo '{"foo":"bar"}' | sp -f yaml
    > ---
    > foo: bar

    $ echo '{"foo":"bar"}' | sp -f minjson
    > {"foo":"bar"}
